#!/bin/bash
SCRIPTFILE="$(realpath "${BASH_SOURCE}")"
cd "${SCRIPTFILE%/*}"
rm -rf src/spp
git clone https://github.com/radare/spp.git src/spp
rm -rf src/spp/.git
echo done
